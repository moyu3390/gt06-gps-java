# gt06-gsp-java

#### 介绍
卫星GPS定位器GT06协议socket通讯JAVA Spring Boot对接

对接在网上买的GPS定位设备

用于实施人员位置查看，可轨迹记录（百度鹰眼或者高德猎鹰）


#### 终端测试

测试数据
```
登陆包:
78780D01035341353215036200022D060D0A
状态包:
78780A134B040300010011634F0D0A
GPS数据包:
78781F120B081D112E10CF027AC7EB0C46584900148F01CC00287D001FB8000380810D0A
GPS&LBS合并信息包:
787825160B0B0F0E241DCF027AC8870C4657E60014020901CC00287D001F726506040101003656A40D0A
```

测试工具：[卓岚TCP UDP调试助手](http://zlmcu.com/document/tcp_debug_tools.html)

- 登录

![登录](https://gitee.com/mr-xiaoyu/gt06-gps-java/raw/master/images/login.jpg)

- 状态

![状态](https://gitee.com/mr-xiaoyu/gt06-gps-java/raw/master/images/state.jpg)

- GPS

![GPS](https://gitee.com/mr-xiaoyu/gt06-gps-java/raw/master/images/gps.jpg)

- GPS&状态

![GPS&状态](https://gitee.com/mr-xiaoyu/gt06-gps-java/raw/master/images/gps+state.jpg)

- 设备实测

![设备实测](https://gitee.com/mr-xiaoyu/gt06-gps-java/raw/master/images/test_1.jpg)