package club.mrxiao.gps.service;

import club.mrxiao.baidu.domain.BaiduTraceTrackPoint;
import club.mrxiao.baidu.exception.BaiduTraceException;
import club.mrxiao.baidu.response.BaiduTraceTrackAddPointsResponse;

import java.util.List;

/**
 * 轨迹点管理相关接口
 * @author xiaoyu
 */
public interface TrackManageService {

    /**
     * 百度鹰眼上传单个轨迹点
     * @param baiduTraceTrackPoint
     * @throws BaiduTraceException
     */
    void baiduTrackAddPoint(BaiduTraceTrackPoint baiduTraceTrackPoint) throws BaiduTraceException;

    /**
     * 百度鹰眼批量上传轨迹点，一次不可超过100个
     * @param baiduTraceTrackPoints
     * @return
     * @throws BaiduTraceException
     */
    BaiduTraceTrackAddPointsResponse baiduTrackAddPoints(List<BaiduTraceTrackPoint> baiduTraceTrackPoints) throws BaiduTraceException;
}
