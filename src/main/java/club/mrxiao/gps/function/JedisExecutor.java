package club.mrxiao.gps.function;

import club.mrxiao.gps.exception.RedisConnectException;

/**
 * @author xiaoyu
 * @param <T>
 * @param <R>
 */
@FunctionalInterface
public interface JedisExecutor<T, R> {

    /**
     * 执行
     * @param t
     * @return
     * @throws RedisConnectException
     */
    R excute(T t) throws RedisConnectException;
}
