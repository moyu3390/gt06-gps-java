package club.mrxiao.gps.dao;

import club.mrxiao.gps.domain.Entity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 终端服务
 * @author xiaoyu
 */

public interface EntityMapper extends BaseMapper<Entity> {
}
