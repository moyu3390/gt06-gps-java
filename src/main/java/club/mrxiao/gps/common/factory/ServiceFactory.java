package club.mrxiao.gps.common.factory;

import club.mrxiao.baidu.domain.BaiduTraceTrackPoint;
import club.mrxiao.gps.common.constant.GpsCacheKeyConstant;
import club.mrxiao.gps.domain.Entity;
import club.mrxiao.gps.exception.RedisConnectException;
import club.mrxiao.gps.service.EntityService;
import club.mrxiao.gps.service.RedisService;
import club.mrxiao.gps.socket.domain.GpsDataInputDTO;
import club.mrxiao.gps.socket.domain.GpsStateInputDto;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * servcie 工厂
 * @author xiaoyu
 */
@Slf4j
@Component
public class ServiceFactory {

    @Autowired
    private EntityService entityService;
    @Autowired
    private RedisService redisService;

    private static ServiceFactory serviceFactory;

    @PostConstruct
    public void init(){
        serviceFactory = this;
        serviceFactory.entityService = this.entityService;
        serviceFactory.redisService = this.redisService;
    }

    public static Entity findEntityByImei(String imei){
        return serviceFactory.entityService.findByImei(imei);
    }


    /**
     * 更新状态
     * @param stateInputDto
     */
    public static void updateState(GpsStateInputDto stateInputDto){
        if(StrUtil.isNotBlank(stateInputDto.getPowerState())){
            String stateStr = JSON.toJSONString(stateInputDto);
            String key = GpsCacheKeyConstant.FACILITY_STATE + stateInputDto.getEntityId();
            try {
                serviceFactory.redisService.set(key,stateStr);
            } catch (RedisConnectException e) {
                log.error("设备状态更新失败",e);
            }
        }
    }

    /**
     * 新增轨迹点
     * @param gpsDataInputDTO
     */
    public static void newPoint(GpsDataInputDTO gpsDataInputDTO){
        BaiduTraceTrackPoint point = toBaiduTraceTrackPoint(gpsDataInputDTO);
        try {
            String pointStr = JSON.toJSONString(point);
            serviceFactory.redisService.zadd(GpsCacheKeyConstant.GPS_DATA_LIST,Double.valueOf(point.getLocTime()),pointStr);
            String key = GpsCacheKeyConstant.CURRENT_LOCATION + gpsDataInputDTO.getEntityId();
            serviceFactory.redisService.set(key,pointStr);
        } catch (RedisConnectException e) {
            log.error("轨迹点存储失败",e);
        }
    }

    private static BaiduTraceTrackPoint toBaiduTraceTrackPoint(GpsDataInputDTO gpsDataInputDTO){
        BaiduTraceTrackPoint point = new BaiduTraceTrackPoint();
        if(gpsDataInputDTO.getLatitude() != null){
            point.setLatitude(gpsDataInputDTO.getLatitude());
        }
        if(gpsDataInputDTO.getLongitude() != null){
            point.setLongitude(gpsDataInputDTO.getLongitude());
        }
        if(gpsDataInputDTO.getSpeed() != null){
            point.setSpeed(gpsDataInputDTO.getSpeed());
        }
        if(gpsDataInputDTO.getCourse() != null){
            point.setDirection(gpsDataInputDTO.getCourse());
        }
        if(StrUtil.isNotBlank(gpsDataInputDTO.getName())){
            point.setEntityName(gpsDataInputDTO.getName());
        }
        point.setLocTime(DateUtil.currentSeconds());
        point.setCoordTypeInput("wgs84");
        return point;
    }
}
