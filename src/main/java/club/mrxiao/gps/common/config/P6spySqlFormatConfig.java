package club.mrxiao.gps.common.config;

import cn.hutool.core.util.StrUtil;
import com.p6spy.engine.spy.appender.MessageFormattingStrategy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 自定义 p6spy sql输出格式
 * @author xiaoyu
 */
public class P6spySqlFormatConfig implements MessageFormattingStrategy {

    /**
     * 过滤掉定时任务的 SQL
     */
    @Override
    public String formatMessage(int connectionId, String now, long elapsed, String category, String prepared, String sql, String url) {
        String lf = "\n";
        String space = " ";
        return StrUtil.isNotBlank(sql) ? formatFullTime(LocalDateTime.now())
                + " | 耗时 " + elapsed + " ms | SQL 语句：" + lf + sql.replaceAll("[\\s]+", space) + ";" : "";
    }

    private String formatFullTime(LocalDateTime localDateTime) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return localDateTime.format(dateTimeFormatter);
    }
}