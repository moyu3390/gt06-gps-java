package club.mrxiao.gps.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Socket配置
 * @author xiaoyu
 */

@Data
@Configuration
@ConfigurationProperties(prefix = "gps.socket")
public class SocketProperties {

    private Integer port;

    private Integer max;
}
