package club.mrxiao.gps.common.constant;

/**
 * 缓存Key常量
 * @author xiaoyu
 */
public class GpsCacheKeyConstant {

    public static final String BASE_KEY = "gps_service:";

    /**
     * gps信息列表
     */
    public static final String GPS_DATA_LIST = BASE_KEY + "list";

    /**
     * 当前位置
     */
    public static final String CURRENT_LOCATION = BASE_KEY + "location:";

    /**
     * 设备状态
     */
    public static final String FACILITY_STATE = BASE_KEY + "state:";

    /**
     * 参数错误列表
     */
    public static final String PARAM_ERROR_LIST = BASE_KEY + "paramErrorlist";

    /**
     * 服务器内部错误列表
     */
    public static final String INTERNAL_ERROR_LIST = BASE_KEY + "internalErrorlist";

}
